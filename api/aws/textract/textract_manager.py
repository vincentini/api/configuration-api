import os
import logging
import boto3
from botocore.exceptions import ClientError

from api.globals import *
from api.models.customer_file import CustomerFile
from api.models.customer_file_data import CustomerFileData
from api.services.customer_file_aws_textractor import process_textract_response


import logging
import boto3
import os
from botocore.exceptions import ClientError


class S3BucketManager:

    @staticmethod
    def create_bucket(s3_bucket_name):
        try:
            s3_client = boto3.client('s3')
            s3_client.create_bucket(Bucket=s3_bucket_name)
            return True
        except ClientError as e:
            logging.error(e)
            return e

    @staticmethod
    def delete_bucket(s3_bucket_name):
        try:
            s3_client = boto3.client('s3')
            s3_client.delete_bucket(Bucket=s3_bucket_name)
            return True
        except ClientError as e:
            logging.error(e)
            return e

    @staticmethod
    def upload_file(file, filename, s3_bucket_name):
        try:
            s3_client = boto3.client('s3')
            s3_client.upload_fileobj(file, s3_bucket_name, filename)
            return True
        except ClientError as e:
            logging.error(e)
            return e

    @staticmethod
    def delete_file(s3_object_name, s3_bucket_name):
        try:
            s3_client = boto3.client('s3')
            s3_client.delete_object(Bucket=s3_bucket_name, Key=s3_object_name)
            return True
        except ClientError as e:
            logging.error(e)
            return e

    @staticmethod
    def get_file_url(s3_object_name, s3_bucket_name):
        try:
            s3_client = boto3.client('s3')
            response = s3_client.generate_presigned_url('get_object',
                                                        Params={'Bucket': s3_bucket_name,
                                                                'Key': s3_object_name},
                                                        ExpiresIn=60)
            return response
        except ClientError as e:
            logging.error(e)
            return e

    @staticmethod
    def download_file(s3_object_name, s3_bucket_name):
        try:
            path = f'{os.path.abspath(os.getcwd())}/{s3_object_name}'

            s3 = boto3.resource('s3')
            s3.meta.client.download_file(s3_bucket_name, s3_object_name, path)

            return path
        except ClientError as e:
            logging.error(e)
            return e

from django.db import models

from api.models.enums import DATA_TYPE


class Table(models.Model):
    table_name = models.TextField(max_length=80, blank=False, null=False, unique=True)
    table_description = models.TextField(max_length=300, blank=True, null=True)

    class Meta:
        ordering = ['table_name']


class Field(models.Model):
    table = models.ForeignKey(Table, on_delete=models.CASCADE, related_name='table')
    field_name = models.TextField(max_length=80, null=False, blank=False)
    field_type = models.IntegerField(choices=DATA_TYPE, null=False)
    field_size = models.IntegerField(null=True)
    field_description = models.TextField(max_length=300, null=True, blank=True)

    class Meta:
        unique_together = ['table', 'field_name']
        ordering = ['field_name']

from django.db import models

from api.models.enums import STATUS


class FileCategory(models.Model):
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=1, choices=STATUS, default='A', null=False, blank=False)

    class Meta:
        ordering = ['name']

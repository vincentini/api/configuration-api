from django.db import models

from api.models.customer_file import CustomerFile


class CustomerFileData(models.Model):
    customer_file = models.ForeignKey(CustomerFile, on_delete=models.CASCADE)
    aws_job_id = models.CharField(max_length=300)
    status = models.CharField(max_length=100)
    job_last_refresh = models.DateTimeField(auto_now=True)
    data = models.TextField()
    meta = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['-id']

from django.db import models
from localflavor.br.models import BRCNPJField
from django.utils.translation import ugettext as _


class Customer(models.Model):
    name = models.CharField(max_length=150, error_messages={
        'required': _('Nome obrigatório')
    })
    cnpj = BRCNPJField(blank=False, null=False, unique=True, error_messages={
        'invalid': _('CNPJ Inválido')
    })

    class Meta:
        ordering = ['name']

from django.db import models

from api.aws.s3.s3_bucket_manager import S3BucketManager


class S3Bucket(models.Model):
    name = models.CharField(max_length=200, unique=True)
    bucket_name = models.CharField(max_length=60, unique=True)
    created = models.BooleanField(default=False)

    def delete(self, using=None, keep_parents=False):
        S3BucketManager.delete_bucket(self.bucket_name)
        super(S3Bucket, self).delete()

    class Meta:
        ordering = ['name']

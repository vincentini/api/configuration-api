from django.db import models

from api.aws.s3.s3_bucket_manager import S3BucketManager
from api.models.customer import Customer
from api.models.file_category import FileCategory
from api.models.s3_bucket import S3Bucket


class CustomerFile(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)
    file_category = models.ForeignKey(FileCategory, on_delete=models.PROTECT, null=True, blank=True)
    s3_bucket = models.ForeignKey(S3Bucket, on_delete=models.PROTECT)
    file_name = models.CharField(max_length=1000, blank=False, null=False)
    file_extension = models.CharField(max_length=10)
    s3_object_name = models.CharField(max_length=1000, unique=True, blank=True, null=True)
    file_description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def delete(self, using=None, keep_parents=False):
        S3BucketManager.delete_file(self.s3_object_name, self.s3_bucket.bucket_name)
        super(CustomerFile, self).delete()

    class Meta:
        ordering = ['-id']

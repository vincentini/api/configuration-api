
STATUS = (
    ('A', 'Ativo'),
    ('I', 'Inativo')
)

DATA_TYPE = (
    (1, 'Texto'),
    (2, 'Inteiro'),
    (3, 'Decimal'),
    (4, 'Percentual'),
    (5, 'Monetário')
)

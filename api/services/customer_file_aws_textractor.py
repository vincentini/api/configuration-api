import json
import time
import threading

from api.models.customer_file import CustomerFile
from api.models.customer_file_data import CustomerFileData
from api.utils.trp import Document

import logging
import boto3
from botocore.exceptions import ClientError

from api.globals import *


def process_as_table(s3_bucket_name, s3_object_name):
    try:
        client = boto3.client('textract', region_name=AWS_REGION)
        response = client.start_document_analysis(
            DocumentLocation={
                'S3Object': {
                    'Bucket': s3_bucket_name,
                    'Name': s3_object_name
                }
            },
            FeatureTypes=[
                'TABLES',
            ],
        )

        return response
    except ClientError as e:
        logging.error(e)
        return e


def start_textract_analysis(customer_file_id):
    cf = CustomerFile.objects.get(pk=customer_file_id)

    response = process_as_table(cf.s3_bucket.bucket_name, cf.s3_object_name)

    cf_data = CustomerFileData()
    cf_data.customer_file = cf
    cf_data.status = 'CREATED'
    cf_data.aws_job_id = response.get('JobId', None)
    cf_data.save()

    t = threading.Thread(target=get_textract_result, args=(cf_data.id,))
    t.start()


def get_textract_result(customer_file_data_id):
    cfd = CustomerFileData.objects.get(pk=customer_file_data_id)
    response = get_table_process_result(job_id=cfd.aws_job_id)
    process_textract_response(customer_file_data_id=customer_file_data_id, response=response)


def process_textract_response(customer_file_data_id, response):
    cfd = CustomerFileData.objects.get(pk=customer_file_data_id)

    status = response['JobStatus']

    print(f'Response from cfd {cfd.id}: {status}')

    if status == 'IN_PROGRESS':
        cfd.status = response['JobStatus']
        cfd.save()
        time.sleep(10)
        get_textract_result(customer_file_data_id)
        return

    if status == 'SUCCEEDED':
        pages = [response]

        next_token = None

        if 'NextToken' in response:
            next_token = response['NextToken']

        while next_token:
            time.sleep(5)

            response = get_table_process_result(job_id=cfd.aws_job_id, next_token=next_token)

            next_token = None

            if response:
                pages.append(response)

                if 'NextToken' in response:
                    next_token = response['NextToken']

        doc = Document(pages)

        data = []
        for page in doc.pages:
            for table in page.tables:
                for r, row in enumerate(table.rows):
                    data_row = {}
                    for c, cell in enumerate(row.cells):
                        data_row[f'column_{c}'] = cell.text.strip()
                    data.append(data_row)

        data_row_json = json.dumps(data)
        cfd.data = data_row_json
        cfd.status = 'FINISHED'
        cfd.save()
    else:
        cfd.status = response['JobStatus']
        cfd.save()


def get_table_process_result(job_id, next_token=None):
    try:

        client = boto3.client('textract', region_name=AWS_REGION)

        if next_token:
            response = client.get_document_analysis(JobId=job_id, NextToken=next_token)
        else:
            response = client.get_document_analysis(JobId=job_id)

        return response
    except ClientError as e:
        logging.error(e)
        return e

def get_file_name(file_name):
    if file_name:
        dot = file_name.rindex('.')
        return file_name[0:dot]
    return file_name


def get_file_extension(file_name):
    if file_name:
        dot = file_name.rindex('.') + 1
        return file_name[dot:len(file_name)]
    return ""

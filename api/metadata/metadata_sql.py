def create_table_sql(table, fields):
    if len(fields) <= 0:
        return ''

    sql = f'CREATE TABLE {table.table_name} ('
    for field in fields:
        sql += f'{field.field_name} {create_field_sql(field)}, '

    if sql.endswith(', '):
        sql = sql[:-2]
    sql += ')'
    return sql


def drop_table_sql(table):
    return f'DROP TABLE {table.table_name}'


def alter_table_add_column(table, field):
    return f'ALTER TABLE {table.table_name} ADD COLUMN {field.field_name} {create_field_sql(field)}'


def alter_table_modify_column(table, field):
    return f'ALTER TABLE {table.table_name} ALTER COLUMN {field.field_name} TYPE {create_field_sql(field)}'


def get_database_type(field):
    switcher = {
        1: 'character varying',
        2: 'integer',
        3: 'numeric',
        4: 'numeric',
        5: 'numeric'
    }
    return switcher.get(field.field_type)


def create_field_sql(field):
    switcher = {
        1: create_field_text(field),
        2: create_field_integer(field),
        3: create_field_decimal(field),
        4: create_field_percentual(field),
        5: create_field_monetary(field)
    }

    return switcher.get(field.field_type)


def create_field_text(field):
    return f'VARCHAR({field.field_size or 255})'


def create_field_integer(field):
    return 'INTEGER'


def create_field_decimal(field):
    return f'NUMERIC({field.field_size or 10, field.field_size or 10})'


def create_field_percentual(field):
    return 'NUMERIC(5, 4)'


def create_field_monetary(field):
    return f'NUMERIC(12, 4)'

from api.metadata.metadata_connections import get_cursor
from api.metadata.metadata_sql import create_table_sql, alter_table_add_column, get_database_type, \
    alter_table_modify_column, drop_table_sql
from api.models.metadata import Table, Field


class MetadataService:
    table = None;
    fields = None;
    table_name = '';

    def __init__(self, table_id):
        self.table = Table.objects.get(pk=table_id)
        self.fields = Field.objects.all().filter(table=self.table.id)
        self.table_name = self.table.table_name.lower()

    @property
    def table_exists(self):
        cursor = get_cursor()
        cursor.execute(f'SELECT * FROM information_schema.tables t WHERE t.table_name = \'{self.table_name}\'')
        row = cursor.fetchone()
        return row is not None

    def persist_table(self):
        if self.table_exists:
            self.update_table()
        else:
            self.create_table()

    def drop_table(self):
        if self.table_exists:
            sql = drop_table_sql(self.table)
            cursor = get_cursor()
            cursor.execute(sql)

    def create_table(self):
        sql = create_table_sql(self.table, self.fields)
        cursor = get_cursor()
        cursor.execute(sql)

    def update_table(self):
        cursor = get_cursor()
        for field in self.fields:

            sql = f'select table_name, ' \
                  f' column_name,	' \
                  f' data_type, ' \
                  f' character_maximum_length, ' \
                  f' numeric_precision, ' \
                  f' numeric_scale from information_schema.columns c ' \
                  f' where c.table_name = \'{self.table_name}\' ' \
                  f'   and c.column_name = \'{field.field_name}\''
            cursor.execute(sql)
            row = cursor.fetchone()
            if row is None:
                self.__create_table_field(field)
            else:
                if not self.__check_column(field, row):
                    self.__alter_table_field(field)

    def __check_column(self, field, row):
        db_type = row[2]
        db_char_size = row[3]
        db_numeric_size = row[4]
        db_numeric_precision = row[5]

        local_db_type = get_database_type(field)

        if db_type != local_db_type:
            return False

        if field.field_type == 1:
            if db_char_size < (field.field_size or 255):
                print(db_char_size)
                print(field.field_size or 255)
                return False
            return True
        if field.field_type == 2:
            return True
        if field.field_type == 3:
            if db_numeric_size < (field.field_size or 10):
                return False
            if db_numeric_precision < (field.field_size or 10):
                return False
            return True
        if field.field_type == 4:
            if db_numeric_size < 5:
                return False
            if db_numeric_precision < 4:
                return False
            return True
        if field.field_type == 5:
            if db_numeric_size < 12:
                return False
            if db_numeric_precision < 4:
                return False
            return True

    def __create_table_field(self, field):
        sql = alter_table_add_column(self.table, field)
        get_cursor().execute(sql)

    def __alter_table_field(self, field):
        sql = alter_table_modify_column(self.table, field)
        print(sql)
        get_cursor().execute(sql)

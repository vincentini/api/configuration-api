from django.db import connections


def get_connection():
    return connections['default']


def get_cursor():
    conn = get_connection()
    return conn.cursor()

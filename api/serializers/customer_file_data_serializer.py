from rest_framework import serializers

from api.models.customer_file_data import CustomerFileData


class CustomerFileDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerFileData
        exclude = ['data']

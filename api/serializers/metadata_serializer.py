from api.models.metadata import Table, Field
from rest_framework import serializers


class FieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = Field
        fields = ['id', 'field_name', 'field_type', 'field_description', 'field_size', 'table']


class TableSerializer(serializers.ModelSerializer):
    table_fields = serializers.SerializerMethodField()

    class Meta:
        model = Table
        fields = ['id', 'table_name', 'table_description', 'table_fields']

    def get_table_fields(self, obj):
        fields = Field.objects.filter(table_id=obj.id)
        result = []
        for f in fields:
            result.append({'id': f.id,
                           'field_name': f.field_name,
                           'field_type': f.field_type,
                           'field_size': f.field_size,
                           'field_description': f.field_description})

        return result

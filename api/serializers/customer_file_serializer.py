from rest_framework import serializers

from api.models.customer_file import CustomerFile
from api.models.customer_file_data import CustomerFileData


class CustomerFileSerializer(serializers.ModelSerializer):
    file_category_name = serializers.SerializerMethodField()
    customer_name = serializers.SerializerMethodField()
    s3_bucket_name = serializers.SerializerMethodField()
    customer_file_data_id = serializers.SerializerMethodField()
    customer_file_data_status = serializers.SerializerMethodField()

    class Meta:
        model = CustomerFile
        fields = '__all__'

    def get_file_category_name(self, obj):
        if obj.file_category is not None:
            return obj.file_category.name
        return ""

    def get_customer_name(self, obj):
        if obj.customer is not None:
            return obj.customer.name
        return ""

    def get_s3_bucket_name(self, obj):
        if (obj.s3_bucket) is not None:
            return obj.s3_bucket.name
        return ""

    def get_customer_file_data_id(self, obj):
        cfd = CustomerFileData.objects.all().filter(customer_file=obj.id).first()

        if cfd:
            return cfd.id
        return None

    def get_customer_file_data_status(self, obj):
        cfd = CustomerFileData.objects.all().filter(customer_file=obj.id).first()

        if cfd:
            return cfd.status
        return None

from django.contrib.auth.models import User
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer


class UserSerializer(ModelSerializer):
    name = SerializerMethodField()

    class Meta:
        model = User
        fields = ['username', 'name', 'first_name', 'last_name', 'email']

    def get_name(self, obj):
        return f'{obj.first_name} {obj.last_name}'

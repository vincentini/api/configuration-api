from rest_framework import serializers

from api.models.file_category import FileCategory


class FileCategorySerializer(serializers.ModelSerializer):
    status_description = serializers.SerializerMethodField()

    class Meta:
        model = FileCategory
        fields = '__all__'

    def get_status_description(self, obj):
        return obj.get_status_display()

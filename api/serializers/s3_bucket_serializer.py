from rest_framework import serializers

from api.models.s3_bucket import S3Bucket


class S3BucketSerializer(serializers.ModelSerializer):
    class Meta:
        model = S3Bucket
        fields = '__all__'

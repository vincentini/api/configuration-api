from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet

from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated

from api.framework.pagination import ApiPagination
from api.metadata.metadata_service import MetadataService
from api.models.metadata import Table, Field
from api.serializers.metadata_serializer import TableSerializer, FieldSerializer

from rest_framework.response import Response


class TableViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Table.objects.all()
    pagination_class = ApiPagination
    serializer_class = TableSerializer
    filter_backends = [SearchFilter]
    search_fields = ['table_name', 'table_description']

    @action(methods=['post'],
            detail=True,
            permission_classes=[IsAuthenticated],
            url_name='create-table',
            url_path='create-table')
    def create_table(self, request, pk=None):
        MetadataService(pk).persist_table()
        return Response(status=204)\


    @action(methods=['post'],
            detail=True,
            permission_classes=[IsAuthenticated],
            url_name='drop-table',
            url_path='drop-table')
    def drop_table(self, request, pk=None):
        MetadataService(pk).drop_table()
        return Response(status=204)


class FieldViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Field.objects.all()
    pagination_class = ApiPagination
    serializer_class = FieldSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['table_id']

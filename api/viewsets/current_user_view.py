from django.contrib.auth.models import User
from rest_framework.response import Response

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from api.serializers.user_serializer import UserSerializer


class CurrentUserView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get(self, request):
        queryset = self.queryset.filter(username=self.request.user)
        serializer = UserSerializer(queryset.get())
        return Response(serializer.data)

import json

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.framework.pagination import ApiPagination

from api.models.customer_file_data import CustomerFileData
from api.serializers.customer_file_data_serializer import CustomerFileDataSerializer


class CustomerFileDataView(viewsets.ReadOnlyModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = CustomerFileData.objects.all()
    serializer_class = CustomerFileDataSerializer
    pagination_class = ApiPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['customer_file']

    @action(methods=['get'],
            detail=True,
            permission_classes=[IsAuthenticated],
            url_name='data',
            url_path='data')
    def get_data_result(self, request, pk=None):
        cfd = CustomerFileData.objects.get(pk=pk)
        json_data = json.loads(cfd.data)
        return Response(status=200, data=json_data)

    @action(methods=['patch'],
            detail=True,
            permission_classes=[IsAuthenticated],
            url_name='update-data',
            url_path='update-data')
    def update_data(self, request, pk=None):
        cfd = CustomerFileData.objects.get(pk=pk)

        data = request.data['data']
        cfd.data = json.dumps(data)
        cfd.save()

        return Response(status=200)

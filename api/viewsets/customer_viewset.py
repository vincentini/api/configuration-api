from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated

from rest_framework.viewsets import ModelViewSet

from api.framework.pagination import ApiPagination
from api.models.customer import Customer
from api.serializers.customer_serializer import CustomerSerializer


class CustomerViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Customer.objects.all()
    pagination_class = ApiPagination
    serializer_class = CustomerSerializer
    filter_backends = [SearchFilter]
    search_fields = ['name', 'cnpj']

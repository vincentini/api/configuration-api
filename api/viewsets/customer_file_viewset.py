from rest_framework import viewsets, permissions, filters, views, parsers, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.aws.s3.s3_bucket_manager import S3BucketManager
from api.framework.pagination import ApiPagination
from api.models.customer import Customer
from api.models.customer_file import CustomerFile
from api.models.customer_file_data import CustomerFileData
from api.models.file_category import FileCategory
from api.models.s3_bucket import S3Bucket
from api.serializers.customer_file_data_serializer import CustomerFileDataSerializer
from api.serializers.customer_file_serializer import CustomerFileSerializer
from api.services.customer_file_aws_textractor import process_as_table, start_textract_analysis
from api.utils.file_utils import get_file_name, get_file_extension
from api.utils.string_utils import get_random_string


class CustomerFileViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = CustomerFile.objects.all()
    pagination_class = ApiPagination
    serializer_class = CustomerFileSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['file_name',
                     'file_extension',
                     'file_description',
                     's3_object_name',
                     'customer__name',
                     'file_category__name']

    @action(methods=['get'],
            detail=True,
            permission_classes=[IsAuthenticated],
            url_name='s3-link',
            url_path='s3-link')
    def get_s3_object_link(self, request, pk=None):
        cf = CustomerFile.objects.get(pk=pk)
        response = S3BucketManager.get_file_url(cf.s3_object_name, cf.s3_bucket.bucket_name)
        return Response(status=200, data=response)

    @action(methods=['get'],
            detail=True,
            permission_classes=[IsAuthenticated],
            url_name='start-analysis-table',
            url_path='start-analysis-table')
    def analyse_file(self, request, pk=None):
        cf = CustomerFile.objects.get(pk=pk)

        response = process_as_table(cf.s3_bucket.bucket_name, cf.s3_object_name)

        cf_data = CustomerFileData()
        cf_data.customer_file = cf
        cf_data.status = 'CREATED'
        cf_data.aws_job_id = response.get('JobId', None)
        cf_data.save()

        serialized = CustomerFileDataSerializer(cf_data);

        return Response(status=200, data=serialized.data)


class CustomerFileUploadView(views.APIView):
    permission_classes = []
    parser_classes = [parsers.MultiPartParser, parsers.FormParser]

    def options(self, request, *args, **kwargs):
        return Response(status=status.HTTP_200_OK, headers={'Access-Control-Allow-Headers': '*'})

    def put(self, request):
        file_obj = request.data['file']
        filename = file_obj.name

        customer_id = request.query_params['customer_id']
        s3_bucket_id = request.query_params['s3_bucket_id']
        file_category_id = request.query_params.get('file_category_id', None)

        s3_bucket = S3Bucket.objects.get(pk=s3_bucket_id)
        customer = Customer.objects.get(pk=customer_id)

        file_category = None
        if file_category_id is not None:
            file_category = FileCategory.objects.get(pk=file_category_id)

        if not s3_bucket.created:
            return Response(status=400, data={'error': f'Bucket {s3_bucket.bucket_name} ainda não foi criado na AWS!'})

        s3_object_name = get_file_name(filename) + '_' + get_random_string(10) + '.' + get_file_extension(filename)

        # Create CustomerFile
        cf = CustomerFile()
        cf.file_name = get_file_name(filename)
        cf.file_extension = get_file_extension(filename)
        cf.customer = customer
        cf.s3_bucket = s3_bucket
        cf.file_category = file_category
        cf.s3_object_name = s3_object_name
        cf.save()

        serializer = CustomerFileSerializer(cf)

        # Upload to S3
        S3BucketManager.upload_file(file=file_obj, filename=s3_object_name, s3_bucket_name=s3_bucket.bucket_name)

        start_textract_analysis(cf.id)

        return Response(status=201,
                        data=serializer.data,
                        headers={'Access-Control-Allow-Headers': '*'})

from rest_framework import viewsets, permissions, filters

from api.framework.pagination import ApiPagination
from api.models.file_category import FileCategory
from api.serializers.file_category import FileCategorySerializer


class FileCategoryViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    queryset = FileCategory.objects.all()
    pagination_class = ApiPagination
    serializer_class = FileCategorySerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']

from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated

from api.aws.s3.s3_bucket_manager import S3BucketManager
from api.framework.pagination import ApiPagination
from api.models.s3_bucket import S3Bucket
from api.serializers.s3_bucket_serializer import S3BucketSerializer


class S3BucketViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = S3Bucket.objects.all()
    pagination_class = ApiPagination
    serializer_class = S3BucketSerializer
    filter_backends = [SearchFilter]
    search_fields = ['name', 'bucket_name']

    @action(methods=['post'],
            detail=True,
            permission_classes=[IsAuthenticated],
            url_name='create_bucket',
            url_path='create-bucket')
    def create_bucket(self, request, pk=None):
        s3_bucket = S3Bucket.objects.get(pk=pk)
        result = S3BucketManager.create_bucket(s3_bucket.bucket_name)

        if type(result) == bool and result:
            s3_bucket.created = True
            s3_bucket.save()
            return Response({'status': 'ok'})
        else:
            return Response({'message': str(result)}, content_type='application/json', status=400)

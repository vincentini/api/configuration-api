import os

AWS_REGION = os.environ.get('AWS_REGION')

if AWS_REGION is None:
    AWS_REGION = 'us-east-1'

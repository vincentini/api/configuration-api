# Generated by Django 3.0.8 on 2020-08-15 20:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_filecategory'),
    ]

    operations = [
        migrations.CreateModel(
            name='S3Bucket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('bucket_name', models.CharField(max_length=60)),
            ],
        ),
    ]

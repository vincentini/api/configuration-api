from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)

from api.viewsets.customer_file_data_viewset import CustomerFileDataView
from api.viewsets.customer_file_viewset import CustomerFileViewSet, CustomerFileUploadView
from api.viewsets.customer_viewset import CustomerViewSet
from api.viewsets.file_category_viewset import FileCategoryViewSet
from api.viewsets.metadata_viewset import TableViewSet, FieldViewSet
from api.viewsets.s3_bucket_viewset import S3BucketViewSet
from api.viewsets.current_user_view import CurrentUserView

router = routers.DefaultRouter()
router.register('customers', CustomerViewSet)
router.register('customer-files', CustomerFileViewSet)
router.register('customer-file-data', CustomerFileDataView)
router.register('file-categories', FileCategoryViewSet)
router.register('s3-buckets', S3BucketViewSet)
router.register('metadata/tables', TableViewSet)
router.register('metadata/fields', FieldViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('api/me', CurrentUserView.as_view()),
    path('api/', include(router.urls)),
    path('api/customer-files-upload/', CustomerFileUploadView.as_view())
]
